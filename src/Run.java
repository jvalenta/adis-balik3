import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import adis.helpers.XmlHelper;

public class Run {
	private final static String _url = "http://localhost:5000/xml-corr";
	private static InputStream _file = null;
	
	public static void main(String[] args) {
		System.out.println(getXMLString(_url));

		_file = getInputStreamXML(_url);
	}
	
	private static String getXMLString(String url) {
		try {
			XmlHelper xmlHelper = new XmlHelper();
			Document xml = xmlHelper.getDocumentFromUrl(url);
			return xmlHelper.getString(xml);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static InputStream getInputStreamXML(String url) {
		try {
			XmlHelper xmlHelper = new XmlHelper();
			Document xml = xmlHelper.getDocumentFromUrl(url);
			return xmlHelper.getInputStream(xml);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
