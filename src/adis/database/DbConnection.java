package adis.database;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;

import adis.helpers.DbConfigParser;
import adis.interfaces.IDbConnection;

public class DbConnection implements IDbConnection {
	private Connection _connection = null;
	private String _connectionString = "";
	private boolean _isConnected = false;
	
	public DbConnection() {
		try {
			Class.forName("com.informix.jdbc.IfxDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		HashMap<String, String> config = getConfigHashMap();
		
		this._connectionString = "jdbc:informix-sqli://" 
				+ config.get("db_host") + ":" 
				+ config.get("db_port") + "/" 
				+ config.get("db_name") + 
				":informixserver=" + config.get("db_server") + 
				";user=" + config.get("db_username") + 
				";password=" + config.get("db_password");
	}
	
	private HashMap<String, String> getConfigHashMap() {
		try {
			DbConfigParser configParser = new DbConfigParser();
			return configParser.getConfigMap();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Connection getConnection() {
		return this._connection;
	}
	
	public void setConnection(Connection connection) {
		this._connection = connection;
	}

	public void connect() {
		try {
			_connection = DriverManager.getConnection(this._connectionString);
			this._isConnected = true;
		} catch(SQLException ex) {
			ex.printStackTrace();
			this._isConnected = false;
		}
	}
	
	public boolean isConnected() {
		return this._isConnected;
	}
	
	public void close() {
		try {
			this._connection.close();
			this._isConnected = false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
