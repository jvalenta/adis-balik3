package adis.test.units;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import adis.database.DbConnection;

public class DbTest {
	
	@Test
    public void connectToInformixDatabase() {
		DbConnection connection = new DbConnection();
		connection.connect();
		assertTrue(connection.isConnected());
		connection.close();
    }
}
