package adis.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;

public class DbConfigParser {
	private Gson _gson = null;
	private BufferedReader _reader = null;
	
	/**
	* Parses a JSON config file for a DB connection. 
	* Default initialization is without any param for the hard-coded path.
	* @throws FileNotFoundException 
	*/
	public DbConfigParser() throws FileNotFoundException {
		String basePath = new File("").getAbsolutePath();
		basePath += "\\files\\db_config.json";

		this._gson = new Gson();
		this._reader = new BufferedReader(new FileReader(basePath));
	}
	
	/**
	* Parses a JSON config file for DB connection.
	* @param reader A buffered reader with the config file.
	*/
	public DbConfigParser(BufferedReader reader) {
		this._gson = new Gson();
		this._reader = reader;
	}
	
	/**
	* Parses a JSON config file for DB connection.
	* @param filePath Specified absolute path for the location of the config file.
	 * @throws FileNotFoundException 
	*/
	public DbConfigParser(String filePath) throws FileNotFoundException {
		this._gson = new Gson();
		this._reader = new BufferedReader(new FileReader(filePath));
	}
	
	/**
	 * Returns the config file as a HashMap<String, String> object.
	 * */
	public HashMap<String, String> getConfigMap() {
		return this._gson.fromJson(_reader, HashMap.class);
	}
}
